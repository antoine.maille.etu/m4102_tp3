package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.CommandeDto;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

public class CommandeResourceTest extends JerseyTest {
	
	private static final Logger LOGGER = Logger.getLogger(IngredientResourceTest.class.getName());
	private PizzaDao daoPizza;
	private IngredientDao daoIngredient;
	private CommandeDao daoCommande;
	private List<Ingredient> ingredients;
	private List<Pizza> pizzas;

	@Override
	protected Application configure() {
		BDDFactory.setJdbiForTests();

		return new ApiV1();
	}

	// Les méthodes setEnvUp() et tearEnvDown() serviront à terme à initialiser la
	// base de données
	// et les DAO

	// https://stackoverflow.com/questions/25906976/jerseytest-and-junit-throws-nullpointerexception
	@Before
	public void setEnvUp() {
		daoPizza = BDDFactory.buildDao(PizzaDao.class);
		daoIngredient= BDDFactory.buildDao(IngredientDao.class);
		daoCommande = BDDFactory.buildDao(CommandeDao.class);
		
		daoPizza.createTablePizzaAndIngredientAssociation();
		daoIngredient.createTable();

		ingredients = new ArrayList<>();
		pizzas = new ArrayList<>();
		daoIngredient.insert(new Ingredient("mozzarella"));
		daoIngredient.insert(new Ingredient("tomate"));
		daoIngredient.insert(new Ingredient("jambon"));
		ingredients.add(daoIngredient.findByName("mozzarella"));
		ingredients.add(daoIngredient.findByName("tomate"));
		ingredients.add(daoIngredient.findByName("jambon"));
		
		Pizza p = new Pizza("Reine");
		p.setIngredients(ingredients);
		
		daoPizza.insert(p);
		
		pizzas.add(daoPizza.findById(p.getId()));
		pizzas.add(daoPizza.findById(p.getId()));
	}

	@After
	public void tearEnvDown() throws Exception {
		daoIngredient.dropTable();
		daoPizza.dropTablePizzaAndIngredientAssociation();
	}
	
	@Test
	public void testGetEmptyList() {
		daoCommande.dropAll();
		daoCommande.createTablePizzaAndAssociation();
		
		// La méthode target() permet de préparer une requête sur une URI.
		// La classe Response permet de traiter la réponse HTTP reçue.
		Response response = target("/commandes").request().get();

		// On vérifie le code de la réponse (200 = OK)
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		// On vérifie la valeur retournée (liste vide)
		// L'entité (readEntity() correspond au corps de la réponse HTTP.
		// La classe jakarta.ws.rs.core.GenericType<T> permet de définir le type
		// de la réponse lue quand on a un type paramétré (typiquement une liste).
		List<CommandeDto> commandes;
		commandes = response.readEntity(new GenericType<List<CommandeDto>>() {
		});

		assertEquals(0, commandes.size());
	}
	
	@Test
	public void testGetExistingCommande() {

		Commande commande = new Commande();
		commande.setName("miam la pizza 1");
		commande.setPizzas(pizzas);
		daoCommande.insert(commande);
		
		System.out.println(daoCommande.getAll());
		
		Response response = target("/commandes").path(commande.getId().toString()).request(MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		Commande result = Commande.fromDto(response.readEntity(CommandeDto.class));
		assertEquals(commande, result);
	}

	@Test
	public void testGetNotExistingCommande() {
		Response response = target("/commandes").path(UUID.randomUUID().toString()).request().get();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
	}
	
	@Test
	public void testGetExistingCommandeName() {
		Commande commande = new Commande();
		commande.setName("miam la pîzza 2");
		commande.setPizzas(pizzas);
		daoCommande.insert(commande);

		Response response = target("/commandes").path(commande.getId().toString()).path("name").request().get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		assertEquals(commande.getName(), response.readEntity(String.class));
	}

	@Test
	public void testGetNotExistingCommandeName() {
		Response response = target("/commandes").path(UUID.randomUUID().toString()).path("name").request().get();

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}
	
	@Test
	public void testDeleteExistingCommande() {
		Commande commande = new Commande();
		commande.setName("miam la pizza 3");
		commande.setPizzas(pizzas);
		daoCommande.insert(commande);
		
		Response response = target("/commandes").path(commande.getId().toString()).request().delete();

		assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

		Commande result = daoCommande.findById(commande.getId());
		assertEquals(result, null);
	}

	@Test
	public void testDeleteNotExistingCommande() {
		Response response = target("/commandes").path(UUID.randomUUID().toString()).request().delete();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}
	
}