package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.IngredientCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.Form;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

/*
 * JerseyTest facilite l'écriture des tests en donnant accès aux
 * méthodes de l'interface jakarta.ws.rs.client.Client.
 * la méthode configure() permet de démarrer la ressource à tester
 */
public class PizzaResourceTest extends JerseyTest {
	private static final Logger LOGGER = Logger.getLogger(PizzaResourceTest.class.getName());
	private PizzaDao daoPizza;
	private IngredientDao daoIngredients;

	@Override
	protected Application configure() {
		BDDFactory.setJdbiForTests();
		return new ApiV1();
	}

	// Les méthodes setEnvUp() et tearEnvDown() serviront à terme à initialiser la
	// base de données
	// et les DAO

	// https://stackoverflow.com/questions/25906976/jerseytest-and-junit-throws-nullpointerexception
	@Before
	public void setEnvUp() {
		daoPizza = BDDFactory.buildDao(PizzaDao.class);
		daoPizza.createTablePizzaAndIngredientAssociation();
		daoIngredients = BDDFactory.buildDao(IngredientDao.class);
		daoIngredients.createTable();
		daoIngredients.insert(new Ingredient("poire"));
		daoIngredients.insert(new Ingredient("chorizo"));
	}

	@After
	public void tearEnvDown() throws Exception {
		daoPizza.dropTablePizzaAndIngredientAssociation();
		daoIngredients.dropTable();
	}




	@Test
	public void testGetExistingPizza() {
		LOGGER.info("testGetExistingPizza");
		Pizza pizza = new Pizza("poire");
		Ingredient ingredient = daoIngredients.findByName("poire");
		Ingredient ingredient2 = daoIngredients.findByName("chorizo");
		pizza.addIngredient(ingredient);
		pizza.addIngredient(ingredient2);
		daoPizza.insert(pizza);

		Response response = target("/pizzas").path(pizza.getId().toString()).request(MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		Pizza result = Pizza.fromDto(response.readEntity(PizzaDto.class));
		assertEquals(pizza, result);
	}

	@Test
	public void testGetExistingPizzaName() {
		LOGGER.info("testGetExistingPizzaName");
		Pizza pizza = new Pizza("poire");
		Ingredient ingredient = daoIngredients.findByName("poire");
		pizza.addIngredient(ingredient);
		daoPizza.insert(pizza);

		Response response = target("/pizzas").path(pizza.getId().toString()).path("name").request().get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		String result = response.readEntity(String.class);
		assertEquals(pizza.getName(), result);
	}

	@Test
	public void testGetNotExistingPizzaName() {
		LOGGER.info("testGetNotExistingPizzaName");
		Response response = target("/pizzas").path(UUID.randomUUID().toString()).path("name").request().get();

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());

	}

	@Test
	public void testGetNotExistingPizza() {
		LOGGER.info("testGetNotExistingPizza");
		Response response = target("/pizzas").path(UUID.randomUUID().toString()).request().get();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
	}

	@Test
	public void testGetEmptyList() {
		LOGGER.info("testGetEmptyList");
		Response response = target("/pizzas").request().get();
		List<PizzaDto> pizzas = response.readEntity(new GenericType<List<PizzaDto>>() {
		});
		assertEquals(0, pizzas.size());
	}

	@Test
	public void testCreateWithForm() {
		LOGGER.info("testCreateWithForm");
		Form form = new Form();
		form.param("name", "chorizo");
		form.param("ingredients", "poire");
		form.param("ingredients", "chorizo");


		Entity<Form> formEntity = Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE);
		Response response = target("pizzas").request().post(formEntity);

		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
		String location = response.getHeaderString("Location");
		String id = location.substring(location.lastIndexOf('/') + 1);
		Pizza result = daoPizza.findById(UUID.fromString(id));

		assertNotNull(result);
	}

	@Test
	public void testCreateWithJson() {
		LOGGER.info("testCreateWithJson");
		PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
		pizzaCreateDto.setName("Chorizo");
		List<Ingredient> ingredients = new ArrayList<>();
		ingredients.add(daoIngredients.findByName("chorizo"));
		ingredients.add(daoIngredients.findByName("poire"));
		pizzaCreateDto.setIngredients(ingredients);

		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));


		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
		String location = response.getHeaderString("Location");
		String id = location.substring(location.lastIndexOf('/') + 1);
		Pizza result = daoPizza.findById(UUID.fromString(id));

		assertNotNull(result);
	}

	@Test
	public void testCreatePizzaAlreadyExists() {
		LOGGER.info("testCreatePizzaAlreadyExists");
		
		Pizza pizza = new Pizza("chorizo");
		Ingredient ingredient = daoIngredients.findByName("chorizo");
		pizza.addIngredient(ingredient);
		daoPizza.insert(pizza);
		PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
		pizzaCreateDto.setName("chorizo");

		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));


		assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());

	}

	@Test
	public void testCreatePizzaWithoutName() {
		LOGGER.info("testCreatePizzaWithoutName");
		PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();

		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

		assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
	}

	@Test
	public void testCreatePizzaWithNotExistingIngredient() {
		LOGGER.info("testCreatePizzaWithNotExistingIngredient");
		PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
		pizzaCreateDto.setName("covid");
		List<Ingredient > ingredients = new ArrayList<>();
		ingredients.add(new Ingredient("covid"));
		pizzaCreateDto.setIngredients(ingredients);

		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));


		assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
	}
	
	@Test
    public void testDeleteExistingPizza() {
		LOGGER.info("testDeleteExistingPizza");
		Pizza pizza = new Pizza("chorizo");
		Ingredient ingredient = daoIngredients.findByName("chorizo");
		pizza.addIngredient(ingredient);
		daoPizza.insert(pizza);
      
      Response response = target("/pizzas").path(pizza.getId().toString()).request().delete();

      assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

      Pizza result = daoPizza.findById(pizza.getId());
      assertEquals(result, null);
   }

   @Test
   public void testDeleteNotExistingPizza() {
	   LOGGER.info("testDeleteNotExistingPizza");
     Response response = target("/pizzas").path(UUID.randomUUID().toString()).request().delete();
     assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
   }










}

