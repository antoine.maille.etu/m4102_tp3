package fr.ulille.iut.pizzaland.dao;

import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;

public interface PizzaDao {

	@SqlUpdate("CREATE TABLE IF NOT EXISTS Pizzas (id VARCHAR(128) PRIMARY KEY, name VARCHAR(128) UNIQUE NOT NULL)")
	void createPizzaTable();

	@SqlUpdate("CREATE TABLE IF NOT EXISTS PizzaIngredientsAssociation ("
			+ "idPizza VARCHAR(128),"
			+ "idIngredient VARCHAR(128), "
			+ "PRIMARY KEY(idPizza, idIngredient),"
			+ "FOREIGN KEY(idPizza) REFERENCES Pizzas(id) ON DELETE CASCADE, "
			+ "FOREIGN KEY(idIngredient) REFERENCES Ingredients(id) ON DELETE CASCADE)")
	void createAssociationTable();

	@Transaction
	default void createTablePizzaAndIngredientAssociation() {
		createAssociationTable();
		createPizzaTable();
	}

	@SqlUpdate("DROP TABLE IF EXISTS Pizzas")
	void dropPizzaTable();

	@SqlUpdate("DROP TABLE IF EXISTS PizzaIngredientsAssociation")
	void dropAssociationTable();

	@Transaction
	default void dropTablePizzaAndIngredientAssociation() {
		dropAssociationTable();
		dropPizzaTable();
	}

	@Transaction
	default void insert(Pizza pizza) {
		insertPizza(pizza);
		for (Ingredient ingredient: pizza.getIngredients()) {
			insertPizzaAssociation(pizza.getId(), ingredient.getId());
		}
	}

	@SqlUpdate("INSERT INTO Pizzas (id, name) VALUES (:id, :name)")
	void insertPizza(@BindBean Pizza pizza);

	@SqlUpdate("INSERT INTO PizzaIngredientsAssociation (idPizza, idIngredient) VALUES (:idPizza, :idIngredient)")
	void insertPizzaAssociation(@Bind("idPizza") UUID idPizza,@Bind("idIngredient") UUID idIngredient );


	//--------------------findPizza----------------------------

	@Transaction
	default Pizza findByName(String name) {
		Pizza pizza = findByNamePizza(name);
		if(pizza != null) {
			pizza.setIngredients(getPizzaIngredients(pizza.getId()));
		}
		return pizza;

	}

	@Transaction
	default Pizza findById(UUID id) {
		Pizza pizza = findByIdPizza(id);
		if(pizza != null) {
		pizza.setIngredients(getPizzaIngredients(pizza.getId()));
		}
		return pizza;
	}


	@SqlQuery("SELECT * FROM Ingredients WHERE id IN (SELECT idIngredient from PizzaIngredientsAssociation where idPizza = :id)")
	@RegisterBeanMapper(Ingredient.class)
	List<Ingredient> getPizzaIngredients(@Bind("id") UUID id);

	@SqlQuery("SELECT * FROM Pizzas WHERE name = :name")
	@RegisterBeanMapper(Pizza.class)
	Pizza findByNamePizza(@Bind("name") String name);

	@SqlQuery("SELECT * FROM Pizzas WHERE id = :id")
	@RegisterBeanMapper(Pizza.class)
	Pizza findByIdPizza(@Bind("id") UUID id);

	//-------------------------getAllPizzas----------------------

	@Transaction 
	default List<Pizza> getAll(){
		List<Pizza> pizzas = getAllPizzas();
		for (Pizza pizza : pizzas) {
			pizza.setIngredients(getPizzaIngredients(pizza.getId()));
		}
		return pizzas;
	}
	@SqlQuery("SELECT * FROM Pizzas")
	@RegisterBeanMapper(Pizza.class)
	List<Pizza> getAllPizzas();
	
	
	//-------------------delete Pizza---------------------------
	
	@SqlUpdate("DELETE FROM Pizzas WHERE id = :id")
    void remove(@Bind("id") UUID id);
}
