package fr.ulille.iut.pizzaland.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.beans.Pizza;

public interface CommandeDao {

	@SqlUpdate("CREATE TABLE IF NOT EXISTS Commandes (id VARCHAR(128) PRIMARY KEY, name VARCHAR UNIQUE NOT NULL);")
	void createCommandesTable();
	
	@SqlUpdate("CREATE TABLE IF NOT EXISTS CommandesPizzasAssociation ("
			+ "idCommande VARCHAR(128) NOT NULL, "
			+ "idPizza VARCHAR(128) NOT NULL,"
			+ "nbPizzas INT NOT NULL DEFAULT 0,"
			+ "PRIMARY KEY(idCommande, idPizza),"
			+ "FOREIGN KEY(idCommande) REFERENCES Commandes(id) ON UPDATE CASCADE ON DELETE CASCADE,"
			+ "FOREIGN KEY(idPizza) REFERENCES Pizzas(id) ON UPDATE CASCADE ON DELETE CASCADE);")
	void createAssociationTable();
	
	@SqlUpdate("DROP TABLE IF EXISTS Commandes;")
	void dropCommandes();
	
	@SqlUpdate("DROP TABLE IF EXISTS CommandesPizzasAssociation;")
	void dropAssociationTable();
	
	@SqlUpdate("INSERT INTO Commandes (id, name) VALUES (:id, :name);")
	void insertCommande(@BindBean Commande commande);
	
	@SqlUpdate("INSERT INTO CommandesPizzasAssociation (idCommande, idPizza, nbPizzas) VALUES (:idCommande, :idPizza, :count);")
	void insertAssociation(@Bind("idCommande") UUID idCommande, @Bind("idPizza") UUID idPizza, @Bind("count") int count);
	
	@SqlUpdate("UPDATE CommandesPizzasAssociation SET nbPizzas = :count;")
	void updateAssociation(@Bind("idCommande") UUID idCommande, @Bind("idPizza") UUID idPizza, @Bind("count") int count);
	
	@SqlUpdate("DELETE FROM Commandes WHERE id = :id;")
	void remove(@Bind("id") UUID id);
	
	@SqlQuery("SELECT nbPizzas FROM CommandesPizzasAssociation WHERE idPizza = :idPizza AND idCommande = :idCommande;")
	int getNbPizzas(@Bind("idCommande") UUID idCommande, @Bind("idPizza") UUID idPizza);

	@SqlQuery("Select * from Commandes;")
	@RegisterBeanMapper(Commande.class)
	List<Commande> getAllCommandes();

	@SqlQuery("SELECT id FROM Pizzas WHERE id IN (SELECT idPizza FROM CommandesPizzasAssociation WHERE idCommande = :idCommande);")
	List<String> getOrderedPizzas(@Bind("idCommande") UUID id);

	@SqlQuery("SELECT * FROM Commandes WHERE id = :id;")
	@RegisterBeanMapper(Commande.class)
	Commande findCommandeById(@Bind("id") UUID id);
	
	@Transaction
	default void createTablePizzaAndAssociation() {
		createCommandesTable();
		createAssociationTable();
	}
	
	@Transaction
	default void dropAll() {
		dropCommandes();
	}

	@Transaction
	default List<Commande> getAll() {
		List<Commande> commandes = getAllCommandes();
		for(Commande c : commandes) {
			c.setPizzas(getPizzas(c.getId()));
		}
		return commandes;
	}

	@Transaction
	default List<Pizza> getPizzas(UUID id) {
		List<String> pizzasId = getOrderedPizzas(id);
		List<Pizza> pizzas = new ArrayList<>();
		int nbPizzas ;
		Pizza p;
		PizzaDao pizzaDao = BDDFactory.buildDao(PizzaDao.class);
		for(String pizza : pizzasId) {
			nbPizzas = getNbPizzas(id, UUID.fromString(pizza));
			p = pizzaDao.findById(UUID.fromString(pizza));
			for(int i = 0 ; i < nbPizzas; i++) {
				pizzas.add(p);
			}
		}
		return pizzas;
	}
	
	@Transaction
	default void insert(Commande c) {
		insertCommande(c);
		
		int count;
		UUID idCommande = c.getId();
		UUID idPizza;
		
		for(Pizza p : c.getPizzas()) {
			idPizza = p.getId();
			try {
				count = getNbPizzas(idCommande, idPizza)+1;
				updateAssociation(idCommande, idPizza, count);
			} catch(IllegalStateException e) {
				count = 1;
				insertAssociation(idCommande, idPizza, count);
			}
		}
	}
	
	@Transaction
	default Commande findById(UUID id) {
		Commande commande = findCommandeById(id);
		if(commande != null) {
			commande.setPizzas(getPizzas(id));
		}
		return commande;
	}
	
}