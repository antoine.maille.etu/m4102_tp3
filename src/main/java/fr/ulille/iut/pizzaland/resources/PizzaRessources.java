package fr.ulille.iut.pizzaland.resources;

import java.net.URI;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;

@Produces("application/json")
@Path("/pizzas")
public class PizzaRessources {
	private static final Logger LOGGER = Logger.getLogger(PizzaRessources.class.getName());

	private PizzaDao pizzaDao;
	private IngredientDao ingredientDao;

	@Context
	public UriInfo uriInfo;

	public PizzaRessources() {
		pizzaDao = BDDFactory.buildDao(PizzaDao.class);
		pizzaDao.createTablePizzaAndIngredientAssociation();
		ingredientDao = BDDFactory.buildDao(IngredientDao.class);

	}


	@GET
	@Produces({ "application/json", "application/xml" })
	public List<PizzaDto> getAll() {
		LOGGER.info("PizzaResource:getAll");

		List<PizzaDto> l = pizzaDao.getAll().stream().map(Pizza::toDto).collect(Collectors.toList());
		LOGGER.info(l.toString());
		return l;
	}

	@GET
	@Path("{id}")
	@Produces({ "application/json", "application/xml" })
	public PizzaDto getOnePizza(@PathParam("id") UUID id) {
		LOGGER.info("getOnePizza(" + id + ")");
		try {
			Pizza pizza = pizzaDao.findById(id);
			LOGGER.info(pizza.toString());
			return Pizza.toDto(pizza);
		} catch (Exception e) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}

	@GET
	@Path("{id}/name")
	@Produces("text/plain")
	public String getPizzaName(@PathParam("id") UUID id) {
		LOGGER.info("getPizzaName(" + id + ")");
		Pizza pizza = pizzaDao.findByIdPizza(id);

		if (pizza == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		return pizza.getName();
	}

	@POST
	@Consumes("application/x-www-form-urlencoded")
	public Response createPizzaWithForm(@FormParam("name") String name, @FormParam("ingredients") List<String> ingredients) {
		LOGGER.info("PizzaResource:createForm  " + name);
		Pizza existing = pizzaDao.findByName(name);
		if (existing != null) {
			throw new WebApplicationException(Response.Status.CONFLICT);
		}


		try {
			Pizza pizza = new Pizza(name);
			for (String nameIngredient : ingredients) {
				Ingredient ingredient = ingredientDao.findByName(nameIngredient);
				if(ingredient == null) {
					throw new WebApplicationException(Response.Status.BAD_REQUEST);
				}
				pizza.addIngredient(ingredient);
			}

			pizzaDao.insert(pizza);
			PizzaDto pizzaDto = Pizza.toDto(pizza);

			URI uri = uriInfo.getAbsolutePathBuilder().path("" + pizza.getId()).build();

			return Response.created(uri).entity(pizzaDto).build();
		} catch (Exception e) {
			throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
		}
	}

	@POST
	@Consumes("application/json")
	public Response createPizzaJson(PizzaCreateDto dto) {
		LOGGER.info("PizzaResource:createJSON");
		if(dto.getName() == null) {
			throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
		}
		Pizza existing = pizzaDao.findByName(dto.getName());
		if (existing != null) {
			LOGGER.info(existing.toString());
			throw new WebApplicationException(Response.Status.CONFLICT);
		}

		try {
			Pizza pizza = new Pizza(dto.getName());
			for (Ingredient ingredient : dto.getIngredients()) {
				if(ingredientDao.findByName(ingredient.getName()) == null) {
					throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
				}
			}
			pizza.setIngredients(dto.getIngredients());
			pizzaDao.insert(pizza);

			PizzaDto pizzaDto = Pizza.toDto(pizza);

			URI uri = uriInfo.getAbsolutePathBuilder().path("" + pizza.getId()).build();

			return Response.created(uri).entity(pizzaDto).build();
		} catch (Exception e) {
			e.printStackTrace();
			throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
		}
	}
	
	@DELETE
	@Path("{id}")
	public Response deleteIngredient(@PathParam("id") UUID id) {
		if ( pizzaDao.findById(id) == null ) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		pizzaDao.remove(id);

		return Response.status(Response.Status.ACCEPTED).build();
	}




}
