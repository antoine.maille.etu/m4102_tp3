package fr.ulille.iut.pizzaland.resources;

import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.dao.CommandeDao;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.CommandeDto;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;

@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Path("/commandes")
public class CommandeResource {
	
	private static final Logger LOGGER = Logger.getLogger(IngredientResource.class.getName());
	
	private CommandeDao commandes;
	private PizzaDao pizzas;
	private IngredientDao daoIngredients;

	@Context
	public UriInfo uriInfo;

	public CommandeResource() {
		daoIngredients = BDDFactory.buildDao(IngredientDao.class);
		daoIngredients.createTable();

		pizzas = BDDFactory.buildDao(PizzaDao.class);
		pizzas.createTablePizzaAndIngredientAssociation();
		
		commandes = BDDFactory.buildDao(CommandeDao.class);
		commandes.createTablePizzaAndAssociation();
	}
	
	@GET
	public List<CommandeDto> getAll() {
		LOGGER.info("CommandeRessource:getAll");

		List<CommandeDto> l = commandes.getAll().stream().map(Commande::toDto).collect(Collectors.toList());
		LOGGER.info(l.toString());
		return l;
	}
	
	@Path("{id}")
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public CommandeDto getOneCommande(@PathParam("id") UUID id) {
		LOGGER.info("getOneCommande(" + id + ")");
		try {
			Commande commande = commandes.findById(id);
			LOGGER.info(commande.toString());
			return Commande.toDto(commande);
		} catch (Exception e) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}
	
	@GET
	@Path("{id}/name")
	public String getCommandeName(@PathParam("id") UUID id) {
		Commande commande = commandes.findById(id);

		if (commande == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		return commande.getName();
	}
	
	@DELETE
	@Path("{id}")
	public Response deleteCommande(@PathParam("id") UUID id) {
		if ( commandes.findById(id) == null ) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		commandes.remove(id);

		return Response.status(Response.Status.ACCEPTED).build();
	}
	
}
