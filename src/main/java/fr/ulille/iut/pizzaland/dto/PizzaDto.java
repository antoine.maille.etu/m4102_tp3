package fr.ulille.iut.pizzaland.dto;

import java.util.List;
import java.util.UUID;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import jakarta.xml.bind.annotation.XmlRootElement;
@XmlRootElement
public class PizzaDto {
	
	private UUID id;
	private String name;
	private List<Ingredient> ingredients;

	public void setId(UUID id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}

	public UUID getId() {
		return this.id;
	}

	public String getName() {
		return this.name;
	}

	public List<Ingredient> getIngredients() {
		return this.ingredients;
	}

}

